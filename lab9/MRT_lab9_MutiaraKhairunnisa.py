# Mutiara Khairunnisa
# 2106751101
# DDP 1 A
# Lab 9

from datetime import datetime

# Mendefinisikan Variable
jumlah_karyawan = dict()
cash = 0
kue = 0
deterjen = 0
terakhir_dibersihkan = None

class Karyawan:
    def __init__(self, username, password, nama, umur, role) -> None:
        self._uname = username
        self._passw = password
        self._name = nama
        self._umur = umur
        self._role = role
        self._login_terakhir = None
        
    def login(self, password) -> bool:
        self._login_terakhir = datetime.now()
        return self._passw == password

    def __str__(self) -> str:
        return f"""===================================
Username: {self._uname}
Nama: {self._name}
Umur: {self._umur}
Login Terakhir: {self._login_terakhir}
Role: {self._role}"""


class Kasir(Karyawan):
    def __init__(self, username, password, nama, umur, role) -> None:
        super().__init__(username, password, nama, umur, role)
        self._uang_diterima = 0

    def bayar(self, jumlah_pembayaran):
        self._uang_diterima += jumlah_pembayaran
        global cash
        cash += jumlah_pembayaran
        print(f"Berhasil menerima uang sebanyak {jumlah_pembayaran}")

    def kerja(self):
        print(f"Selamat datang {self._name}")
        while(True):
            print("""            
Apa yang ingin anda lakukan? (Tulis angka saja)
3. Terima pembayaran
10. Logout
""")
            pilihan = input("pilihan: ")
            if(not pilihan.isnumeric()):
                print("Tulis angka saja")
                continue
            
            pilihan = int(pilihan)
            if(pilihan == 1):
                print("Anda harus logout sebelum dapat menjalankan fungsi ini")
                continue
            elif(pilihan == 2):
                print("Anda harus logout sebelum dapat menjalankan fungsi ini")
                continue
            elif(pilihan == 3):
                jumlah_bayar = input("Jumlah pembayaran: ")
                if(not jumlah_bayar.isnumeric()):
                    print("Tulis angka saja")
                    continue
                self.bayar(int(jumlah_bayar))
            elif(pilihan == 4):
                print("Anda harus login sebagai janitor untuk menjalankan fungsi ini")
                continue
            elif(pilihan == 5):
                print("Anda harus login sebagai janitor untuk menjalankan fungsi ini")
                continue
            elif(pilihan == 6):
                print("Anda harus login sebagai chef untuk menjalankan fungsi ini")
                continue
            elif(pilihan == 7):
                print("Anda harus login sebagai chef untuk menjalankan fungsi ini")
                continue
            elif(pilihan == 8):
                print("Anda harus logout sebelum dapat menjalankan fungsi ini")
                continue
            elif(pilihan == 9):
                print("Anda harus logout sebelum dapat menjalankan fungsi ini")
                continue
            elif(pilihan == 10):
                print("LOGOUT BERHASIL")
                break
            elif(pilihan == 11):
                print("Anda harus logout sebelum dapat menjalankan fungsi ini")
                continue
            
    def __str__(self) -> str:
        return super().__str__() + f"""
Jumlah uang diterima: {self._uang_diterima}
==================================="""

class Janitor(Karyawan):
    def __init__(self, username, password, nama, umur, role) -> None:
        super().__init__(username, password, nama, umur, role)
        self._bersihkan_toko = 0
        self._beli_deterjen = 0


    def kerja(self):
        print(f"Selamat datang {self._name}")
        while(True):
            print("""
Apa yang ingin anda lakukan? (Tulis angka saja)
4. Bersihkan toko
5. Beli deterjen
10. Logout
""")
            pilihan = input("pilihan: ")
            if(not pilihan.isnumeric()):
                print("Tulis angka saja")
                continue
            
            pilihan = int(pilihan)
            if(pilihan == 1):
                print("Anda harus logout sebelum dapat menjalankan fungsi ini")
                continue
            elif(pilihan == 2):
                print("Anda harus logout sebelum dapat menjalankan fungsi ini")
                continue
            elif(pilihan == 3):
                print("Anda harus login sebagai kasir untuk menjalankan fungsi ini")
                continue
            elif(pilihan == 4):
                self.bersihkan()
                continue
            elif(pilihan == 5):
                jumlah = input("Jumlah pembelian deterjen: ")
                if(not jumlah.isnumeric()):
                    print("Tulis angka saja")
                    continue
                self.beli_deterjen(int(jumlah))
                continue
            elif(pilihan == 6):
                print("Anda harus login sebagai chef untuk menjalankan fungsi ini")
                continue
            elif(pilihan == 7):
                print("Anda harus login sebagai chef untuk menjalankan fungsi ini")
                continue
            elif(pilihan == 8):
                print("Anda harus logout sebelum dapat menjalankan fungsi ini")
                continue
            elif(pilihan == 9):
                print("Anda harus logout sebelum dapat menjalankan fungsi ini")
                continue
            elif(pilihan == 10):
                print("LOGOUT BERHASIL")
                break
            elif(pilihan == 11):
                print("Anda harus logout sebelum dapat menjalankan fungsi ini")
                continue
    
    def bersihkan(self):
        global terakhir_dibersihkan, deterjen
        if(deterjen < 1):
            print("Diperlukan setidaknya 1 deterjen untuk melakukan pembersihan")
            return
        
        self._bersihkan_toko += 1
        deterjen -= 1
        terakhir_dibersihkan = datetime.now()

    def beli_deterjen(self, jumlah):
        self._beli_deterjen += jumlah
        global deterjen
        deterjen += jumlah
        print(f"Berhasil membeli {jumlah} deterjen")

    def __str__(self) -> str:
        return super().__str__() + f"""
Jumlah berapa kali membersihkan toko: {self._bersihkan_toko}
Jumlah deterjen yang telah dibeli: {self._beli_deterjen}
==================================="""

class Chef(Karyawan):
    def __init__(self, username, password, nama, umur, role) -> None:
        super().__init__(username, password, nama, umur, role)
        self._jumlah_kue_dibuat = 0
        self._jumlah_kue_dibuang = 0

    def kerja(self):
        print(f"Selamat datang {self._name}")
        while(True):
            print("""
Apa yang ingin anda lakukan? (Tulis angka saja)
4. Bersihkan toko
5. Beli deterjen
10. Logout
""")
            pilihan = input("pilihan: ")
            if(not pilihan.isnumeric()):
                print("Tulis angka saja")
                continue
            
            pilihan = int(pilihan)
            if(pilihan == 1):
                print("Anda harus logout sebelum dapat menjalankan fungsi ini")
                continue
            elif(pilihan == 2):
                print("Anda harus logout sebelum dapat menjalankan fungsi ini")
                continue
            elif(pilihan == 3):
                print("Anda harus login sebagai kasir untuk menjalankan fungsi ini")
                continue
            elif(pilihan == 4):
                print("Anda harus login sebagai janitor untuk menjalankan fungsi ini")
                continue
            elif(pilihan == 5):
                print("Anda harus login sebagai janitor untuk menjalankan fungsi ini")
                continue
            elif(pilihan == 6):
                jumlah = input("Buat berapa kue?: ")
                if(not jumlah.isnumeric()):
                    print("Tulis angka saja")
                    continue
                self.buat_kue(int(jumlah))
                continue
            elif(pilihan == 7):
                jumlah = input("Buat berapa kue?: ")
                if(not jumlah.isnumeric()):
                    print("Tulis angka saja")
                    continue
                self.buat_kue(int(jumlah))
                continue
            elif(pilihan == 8):
                print("Anda harus logout sebelum dapat menjalankan fungsi ini")
                continue
            elif(pilihan == 9):
                print("Anda harus logout sebelum dapat menjalankan fungsi ini")
                continue
            elif(pilihan == 10):
                print("LOGOUT BERHASIL")
                break
            elif(pilihan == 11):
                print("Anda harus logout sebelum dapat menjalankan fungsi ini")
                continue

    def buat_kue(self, jumlah):
        self._jumlah_kue_dibuat += jumlah
        global kue
        kue += jumlah
        print(f"Berhasil membuat {jumlah} kue")
    
    def buang_kue(self, jumlah):
        global kue
        
        if(jumlah > kue):
            print("Tidak bisa membuang lebih banyak kue dibandingkan dengan stok kue yang ada sekarang")
            return

        self._jumlah_kue_dibuang += jumlah
        kue -= jumlah
        
    def __str__(self) -> str:
        return super().__str__() + f"""
Jumlah kue yang telah dibuat: {self._jumlah_kue_dibuat}
Jumlah kue yang telah dibuang: {self._jumlah_kue_dibuang}
==================================="""