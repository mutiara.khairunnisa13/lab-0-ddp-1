# Nama : Mutiara Khairunnisa
# NPM : 2106751101 
# Kelas : DDP 1-A
# Lab 5


import random 

print("Selamat datang di Matrix Calculator. Berikut adalah operasi-operasi yang dapat dilakukan:")

# kerangka operasi
operasi = """ 1. Penjumlahan
2. Pengurangan
3. Transpose
4. Determinan
5. Keluar """


while True : 
    pilih_operasi = int(pilih_operasi) # agar pada setiap pilih operasi adalah integer dan tdk string 
    print(operasi)
    pilih_operasi = input("Silakan pilih operasi : ")

# Jika user memilih operasi 1 (Penjumlahan)
    if pilih_operasi == 1: 
        ukuran_matriks = input("Ukuran matriks : ")
        baris1_martiks1 = input("Baris 1 matriks 1 : ")
        baris2_martiks1 = input("Baris 2 matriks 1 : ")
        print()
        baris1_martiks2 = input("Baris 1 matriks 2 : ")
        baris2_matriks2 = input("Baris 2 matriks 2 : ")

# Jika user memilih operasi 2 (Pengurangan)
    elif pilih_operasi == 2: 
        ukuran_matriks = input("Ukuran matriks : ")
        baris1_martiks1 = input("Baris 1 matriks 1 : ")
        baris2_martiks1 = input("Baris 2 matriks 1 : ")
        print()
        baris1_martiks2 = input("Baris 1 matriks 2 : ")
        baris2_matriks1 = input("Baris 2 matriks 2 : ")

# Jika user memilih operasi 3 (Transpose)
    elif pilih_operasi == 3: 
        baris1_martiks = input("Baris 1 matriks : ")
        baris2_martiks = input("Baris 2 matriks : ")
        baris3_martiks = input("Baris 3 matriks : ")

# Jika user memilih operasi 4 (Determinan)
    elif pilih_operasi == 4: 
        baris1_martiks = input("Baris 1 matriks : ")
        baris2_martiks = input("Baris 2 matriks : ")

# Jika user memilih operasi 5 (Keluar)
    else:
        print()
        print("Sampai Jumpa!")
        exit()   #Program Berakhir/operasi selesai