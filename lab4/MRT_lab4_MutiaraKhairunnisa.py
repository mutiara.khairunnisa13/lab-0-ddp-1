# Nama  : Mutiara Khairunnisa
# NPM   : 2106751101
# Kelas : DDP 1-A
# Lab 04

# Import kata
import string 

# Memasukkan nama file iput
file_in = input("Masukkan nama file input: ")
file_out = input("Masukkan nama file output")
print()

# Menguji keberadaan file 
try :
    file_input= open(file_in, "r")
    file_output = open(file_out, "w")
    file_content = file_input.read()

# Jika file yang diinput tidak ditemukan    
except FileNotFoundError :
    print("Nama file yang anda masukkan tidak ditemukan :(")

# untuk memindahkan pointer pada file_input pada awal file
file_input.seek(0) 

# Untuk memisahkan isi file 
isi_file = file_content.lower().split() # Untuk memperkecil huruf dan memisahkan kata per kata 
for i in range(len(isi_file)) : 
    isi_file[i] = isi_file[i].strip(string.punctuation) # Untuk setiap index pada isi_file maka tanda bacanya diapus terlebih dahulu

for line in file_input: # untuk mengecek isi file per baris dan memasukkan file output 
    for word in line.split(): # untuk mengecek per kata pada baris 
        
hitung_tanda = file_content.count(string.punctuation)

# total_baris_input =
# total_baris_disimpan = 


file_input.close()
file_output.close()
