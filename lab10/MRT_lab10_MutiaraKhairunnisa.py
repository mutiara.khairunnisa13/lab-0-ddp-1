# Nama : Mutiara Khairunnisa
# NPM  : 2106751101
# Kelas : DDP 1-A
# Lab 10 

from tkinter import *
from tkinter.filedialog import asksaveasfilename, askopenfilename
from tkinter.scrolledtext import *


# TODO: Lengkapi class Application dibawah ini
class Application(Frame):
    def __init__(self, master=None):
        super().__init__(master)
        self.master = master
        self.initUI()
        self.create_buttons()
        self.create_editor()
        self.master.bind_all('<Control-o>', self.load_file_event)
        self.master.bind_all('<Control-s>', self.save_file_event)

    def initUI(self):
        # TODO: Atur judul dan ukuran dari main window,
        # lalu buat sebuah Frame sebagai anchor dari seluruh button
        #membuat judul
        self.master.title("Pacil Editor")
        
        #membuat frame
        self.frame_atas = Frame(self.master)
        self.frame_atas.pack(fill=BOTH)
        self.frame_bawah = Frame(self.master)
        self.frame_bawah.pack(fill=BOTH, expand=True)

        pass

    def create_buttons(self):
        # TODO: Implementasikan semua button yang dibutuhkan
        self.open_file_button = Button(self.frame_atas,text= 'Open File', command = self.load_file)
        self.open_file_button.grid(row =0, pady= 5 , padx = 5, column= 0, sticky = NW)
        self.save_file_button = Button(self.frame_atas,text= 'Save File', command = self.save_file)
        self.save_file_button.grid(row =0, pady= 5 , padx = 5, column= 1, sticky = NW)
        self.exit_button = Button(self.frame_atas, text = 'Quit Program', command = self.master.destroy)
        self.exit_button.grid(row =0, pady= 5 , padx = 5, column= 2, sticky = NW)

    def create_editor(self):
        # TODO: Implementasikan textbox
        self.text = ScrolledText(self.frame_bawah)
        self.text.pack(fill=BOTH, expand=True)

    def load_file_event(self, event):
        self.load_file()

    def load_file(self):
        file_name = askopenfilename(
            filetypes=[("All files", "*")]
        )
        if not file_name:  # Jika pengguna membatalkan dialog, langsung return
            return
        text_file = open(file_name, 'r', encoding="utf-8")
        result = text_file.read()

        # TODO: tampilkan result di textbox
        self.text.delete('1.0', END)
        self.text.insert(INSERT, result)
        self.text.mark_set(INSERT, '1.0')
        self.text.focus()

    def save_file_event(self, event):
        self.save_file()

    def save_file(self):
        file_name = asksaveasfilename(
            filetypes=[("All files", "*")]
        )
        if not file_name:  # Jika pengguna membatalkan dialog, langsung return
            return
        # TODO: ambil isi dari textbox lalu simpan dalam file dengan nama file_name
        file_save = open(file_name, 'w')
        file_save.write(self.text.get("1.0", END))
        file_save.close()

    def set_text(self, text=''):
        self.edit.delete('1.0', END)
        self.edit.insert('1.0', text)
        self.edit.mark_set(INSERT, '1.0')
        self.edit.focus()

    def get_text(self):
        return self.edit.get('1.0', END+'-1c')


if __name__ == "__main__":
    root = Tk()
    app = Application(master=root)
    app.mainloop()
