harga_pulpen = 6000
harga_pensil = 3000
harga_cortape = 14000

print("Toko ini menjual : ")
print("1. Pulpen : " + str(harga_pulpen) + "/pcs")
print("1. Pensil: " + str(harga_pensil) + "/pcs")
print("1. Correction Tape : " + str(harga_cortape) + "/pcs")

print("\nMasukkan jumlah barang yang ingin anda beli")
jumlah_pulpen = int(input("Pulpen : "))
jumlah_pensil = int(input("Pensil : "))
jumlah_cortape = int(input("Correction Tape : "))

total_harga = ((jumlah_pulpen*harga_pulpen) + (jumlah_pensil*harga_pulpen) + (jumlah_cortape*harga_cortape))

print("\nRingkasan Pembelian :")
print(f"{jumlah_pulpen} pulpen x {harga_pulpen}")
print(f"{jumlah_pensil} pensil x {harga_pensil}")
print(f"{jumlah_cortape} correction tape x {harga_cortape}")
print(f"\nTotal harga : {total_harga}")